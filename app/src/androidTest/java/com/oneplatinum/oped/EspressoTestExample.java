package com.oneplatinum.oped;

import android.content.Intent;
import android.net.Uri;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.TextView;

import com.oneplatinum.oped.Activity.DepartmentListingActivity;
import com.oneplatinum.oped.Activity.SplashActivity;
import com.oneplatinum.oped.data.DataHolder;
import com.oneplatinum.oped.model.Department;
import com.oneplatinum.oped.model.Employee;
import com.oneplatinum.oped.utils.Utils;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Rajen on 4/3/2017.
 */

@RunWith(AndroidJUnit4.class)
public class EspressoTestExample {

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule =
            new ActivityTestRule<>(SplashActivity.class);
//            new ActivityTestRule<>(SplashActivity.class, false, true);

    @Rule
    public IntentsTestRule<DepartmentListingActivity> mIntentsTestRule =
            new IntentsTestRule<>(DepartmentListingActivity.class);

    @Test
    public void viewEmployeeDetails() throws InterruptedException {

        mActivityTestRule.launchActivity(null);
//        mIntentsTestRule.launchActivity(null);

        onView(withIndex(withId(R.id.department_container), 0)).perform(click());

        onView(withIndex(withId(R.id.user_image), 0)).perform(click());

        synchronized (this) {
            wait(10000);
        }
    }


    @Test
    public void callEmployeeNumber() throws InterruptedException {
        String VALID_PHONE_NUMBER = "984-123-4567";

        Uri INTENT_DATA_PHONE_NUMBER = Uri.parse("tel:" + VALID_PHONE_NUMBER);

        String PACKAGE_ANDROID_DIALER = "com.android.phone";

        mActivityTestRule.launchActivity(null);
//        mIntentsTestRule.launchActivity(null);
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.department_view), 4)).perform(scrollTo());
        onView(withIndex(withId(R.id.department_view), 4)).perform(click());
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.user_image), 0)).perform(click());
        synchronized (this) {
            wait(3000);
        }
        onView(withId(R.id.user_phone)).perform(scrollTo(), click());

        intended(allOf(
                hasAction(Intent.ACTION_DIAL)
        ));

    }


    @Test
    public void verifyEmployeeEmail() throws InterruptedException {

        mActivityTestRule.launchActivity(null);
//        mIntentsTestRule.launchActivity(null);
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.department_view), 4)).perform(scrollTo());
        onView(withIndex(withId(R.id.department_view), 4)).perform(click());
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.user_image), 0)).perform(click());
        synchronized (this) {
            wait(3000);
        }

        String employeeEmail = getText(withId(R.id.user_email));
        boolean isValid = Utils.isEmailValid(employeeEmail);

        Assert.assertTrue(isValid);
    }

    @Test
    public void verifyEmployeeDOBFormat() throws InterruptedException {

        mActivityTestRule.launchActivity(null);
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.department_view), 4)).perform(scrollTo());
        onView(withIndex(withId(R.id.department_view), 4)).perform(click());
        synchronized (this) {
            wait(3000);
        }
        onView(withIndex(withId(R.id.user_image), 0)).perform(click());
        synchronized (this) {
            wait(3000);
        }

        String employeeEmail = getText(withId(R.id.user_dob));
        Assert.assertTrue(checkDate(employeeEmail));
    }

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }

    public String getText(final Matcher<View> matcher) {
        final String[] stringHolder = {null};
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView) view; //Save, because of check in getConstraints()
                stringHolder[0] = tv.getText().toString();
            }
        });
        return stringHolder[0];
    }

    private boolean checkDate(String datetest) {

//        String dateREG = "EEE MMM dd yyyy";

        String re1 = "((?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|Tues|Thur|Thurs|Sun|Mon|Tue|Wed|Thu|Fri|Sat))";    // Day Of Week 1
        String re2 = "(\\s+)";    // White Space 1
        String re3 = "((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))";    // Month 1
        String re4 = "(\\s+)";    // White Space 2
        String re5 = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])";    // Day 1
        String re6 = "(\\s+)";    // White Space 3
        String re7 = "((?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3})))(?![\\d])";    // Year 1

        Pattern p = Pattern.compile(re1 + re2 + re3 + re4 + re5 + re6 + re7, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        java.util.regex.Matcher m = p.matcher(datetest);
        if (m.find()) {
            return true;
        } else {
            return false;
        }
    }


    @Test
    public void testDepartmentSize() throws InterruptedException {
        ArrayList<Department> testDepart;

        mActivityTestRule.launchActivity(null);
        mActivityTestRule.getActivity().fetchCallback();
        synchronized (this) {
            wait(3000);
        }
        testDepart = DataHolder.departmentList;

        Assert.assertTrue(testDepart != null);
        Assert.assertTrue(testDepart.size() > 0);
//        Assert.assertTrue(testDepart.size() == 5);
    }

    /**
     * Asserts Department image, Name and employee availability
     */
    @Test
    public void testDepartMentModelVerification() throws InterruptedException {
        ArrayList<Department> testDepart = new ArrayList<>();

        mActivityTestRule.launchActivity(null);
        mActivityTestRule.getActivity().fetchCallback();
        synchronized (this) {
            wait(3000);
        }
        testDepart = DataHolder.departmentList;
        Assert.assertTrue(testDepart.size() > 0);

        for (int i = 0; i < testDepart.size(); i++) {
            Assert.assertTrue(testDepart.get(i).image.length() > 0);
            Assert.assertTrue(testDepart.get(i).name.length() > 0);
            Assert.assertTrue(testDepart.get(i).employeeList.size() > 0);
        }
    }


    /**
     * Asserts employee details
     */
    @Test
    public void testDepartMentEmployeeModelVerification() throws InterruptedException {
        ArrayList<Department> departList;
        ArrayList<Employee> employeeList;

        mActivityTestRule.launchActivity(null);
        mActivityTestRule.getActivity().fetchCallback();


        synchronized (this) {
            wait(3000);
        }
        departList = DataHolder.departmentList;
        Assert.assertTrue(departList.size() > 0);

        for (int i = 0; i < departList.size(); i++) {
            employeeList = departList.get(i).employeeList;
            Assert.assertTrue(employeeList.size() > 0);

            for (int j = 0; j < employeeList.size(); j++) {
                Employee singleEmployeeModel = employeeList.get(j);

                Assert.assertTrue(singleEmployeeModel.address.length() > 0);
                Assert.assertTrue(singleEmployeeModel.designation.length() > 0);
                Assert.assertTrue(singleEmployeeModel.dob.length() > 0);
                Assert.assertTrue(singleEmployeeModel.email.length() > 0);
                Assert.assertTrue(singleEmployeeModel.gender.length() > 0);
                Assert.assertTrue(singleEmployeeModel.image.length() > 0);
                Assert.assertTrue(singleEmployeeModel.name.length() > 0);
                Assert.assertTrue(singleEmployeeModel.phone.length() > 0);
            }
        }

    }

}
