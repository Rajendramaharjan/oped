package com.oneplatinum.oped;


import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void testAddition() throws Exception {
        assertEquals(4, 2 + 2);
    }

}