package com.oneplatinum.oped.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 3/31/2017.
 */
public class Department implements Serializable {
    public String name, image;
    public ArrayList<Employee> employeeList = new ArrayList<>();
}
