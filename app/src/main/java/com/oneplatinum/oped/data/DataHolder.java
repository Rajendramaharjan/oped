package com.oneplatinum.oped.data;

import com.oneplatinum.oped.model.Department;
import com.oneplatinum.oped.model.Employee;

import java.util.ArrayList;

/**
 * Created by user on 3/31/2017.
 */
public class DataHolder {
    public static ArrayList<Department> departmentList = new ArrayList<>();
    public static String fetchUrl = "https://api.myjson.com/bins/d5err";

    public static ArrayList<Employee> getEmployeeListFromDepartment(String department) {
        for (int i = 0; i < departmentList.size(); i++) {
            if (departmentList.get(i).name.equals(department)) {
                return departmentList.get(i).employeeList;
            }
        }
        return new ArrayList<Employee>();
    }

    public static String STRING_SAMPLE = "{\n" +
            "    \"output\": {\n" +
            "        \"response\": {\n" +
            "            \"department\": [\n" +
            "                {\n" +
            "                    \"name\": \"Developer\",\n" +
            "                    \"employee\": [\n" +
            "                        {\n" +
            "                            \"name\": \"name1\",\n" +
            "                            \"designation\": \"Adeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name2\",\n" +
            "                            \"designation\": \"Adeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name3\",\n" +
            "                            \"designation\": \"IDeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"Administrator\",\n" +
            "                    \"employee\": [\n" +
            "                        {\n" +
            "                            \"name\": \"name1\",\n" +
            "                            \"designation\": \"admin\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name2\",\n" +
            "                            \"designation\": \"admin\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name3\",\n" +
            "                            \"designation\": \"admin\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"QA\",\n" +
            "                    \"employee\": [\n" +
            "                        {\n" +
            "                            \"name\": \"name1\",\n" +
            "                            \"designation\": \"Adeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name2\",\n" +
            "                            \"designation\": \"Adeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"name\": \"name3\",\n" +
            "                            \"designation\": \"IDeveloper\",\n" +
            "                            \"address\": \"city\",\n" +
            "                            \"phone\": \"9898787878\",\n" +
            "                            \"image\": \"\"\n" +
            "                        }\n" +
            "                    ]\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    }\n" +
            "}";
}
