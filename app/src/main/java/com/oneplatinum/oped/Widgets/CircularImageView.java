package com.oneplatinum.oped.Widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CircularImageView extends ImageView {

    private int mBorderWidth = 2;
    private int mViewWidth;
    private int mViewHeight;
    private Bitmap mImageBitmap;
    private Paint mPaint;
    private Paint mPaintBorder;
    private BitmapShader mShader;

    public CircularImageView(Context context) {
        super(context);
        setup();
    }

    public CircularImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public CircularImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }

    private void setup() {
        // init mPaint
        mPaint = new Paint();
        mPaint.setAntiAlias(true);

        mPaintBorder = new Paint();
        setBorderColor(Color.TRANSPARENT);
        mPaintBorder.setAntiAlias(true);
    }

    //	public void setBorderWidth(int mBorderWidth) {
//		this.mBorderWidth = mBorderWidth;
//		this.invalidate();
//	}
//
    public void setBorderColor(int borderColor) {
        if (mPaintBorder != null)
            mPaintBorder.setColor(borderColor);

        this.invalidate();
    }

    private void loadBitmap() {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) this.getDrawable();

        if (bitmapDrawable != null)
//            mImageBitmap = cropCenter(bitmapDrawable.getBitmap());
            if (bitmapDrawable.getBitmap() != null) {
                mImageBitmap = cropCenter(bitmapDrawable.getBitmap());
            } else {
                mImageBitmap = null;
            }
    }

    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas) {
        // load the bitmap
        loadBitmap();

        // init mShader
        if (mImageBitmap != null) {
            mShader = new BitmapShader(Bitmap.createScaledBitmap(mImageBitmap,
                    canvas.getWidth(), canvas.getHeight(), false),
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            mPaint.setShader(mShader);
            int circleCenter = mViewWidth / 2;

            // circleCenter is the x or y of the view's center
            // radius is the radius in pixels of the cirle to be drawn
            // mPaint contains the mShader that will texture the shape
            canvas.drawCircle(circleCenter + mBorderWidth, circleCenter
                    + mBorderWidth, circleCenter + mBorderWidth, mPaintBorder);
            canvas.drawCircle(circleCenter + mBorderWidth, circleCenter
                    + mBorderWidth, circleCenter, mPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = measureWidth(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec, widthMeasureSpec);

        mViewWidth = width - (mBorderWidth * 2);
        mViewHeight = height - (mBorderWidth * 2);

        setMeasuredDimension(width, height);
    }

    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text
            result = mViewWidth;

        }

        return result;
    }

    private int measureHeight(int measureSpecHeight, int measureSpecWidth) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpecHeight);
        int specSize = MeasureSpec.getSize(measureSpecHeight);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text (beware: ascent is a negative number)
            result = mViewHeight;
        }
        return result;
    }

    /**
     * Creates a centered bitmap
     *
     * @param bmp drawable bitmap for centerCrop
     * @return center cropped bitmap of bmp
     * @see {@link ThumbnailUtils#extractThumbnail(Bitmap, int, int)}
     */
    Bitmap cropCenter(Bitmap bmp) {
        int dimension = Math.min(bmp.getWidth(), bmp.getHeight());
        return ThumbnailUtils.extractThumbnail(bmp, dimension, dimension);
    }
}
