package com.oneplatinum.oped.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.oneplatinum.oped.R;
import com.oneplatinum.oped.adapter.EmployeeFlipAdapter;
import com.oneplatinum.oped.adapter.EmployeeListAdapter;
import com.oneplatinum.oped.data.DataHolder;
import com.oneplatinum.oped.model.Employee;
import com.yalantis.flipviewpager.utils.FlipSettings;

public class EmployeeListingActivity extends AppCompatActivity {

    String departmentId;
    ListView mEmployeeListView;
    ImageView listSwitch, gridSwitch, switchListIcon;
    //layout managers
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    //adapter
//    EmployeeAdapter adapter;
    EmployeeListAdapter adapter;
    EmployeeFlipAdapter flipAdapter;
    private boolean mIsGrid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_listing);

        //get the departmentId to get the employee listing
        departmentId = getIntent().getStringExtra("departmentId");

        switchListIcon = (ImageView) findViewById(R.id.listORGridIcon);

        linearLayoutManager = new LinearLayoutManager(EmployeeListingActivity.this);
        gridLayoutManager = new GridLayoutManager(EmployeeListingActivity.this, 2);

        //action bar
//        getActionBar().setDisplayHomeAsUpEnabled(true);
        //initialise all views
        initViews();
    }

    public void initViews() {
        //get the views
        listSwitch = (ImageView) findViewById(R.id.show_in_list);
        gridSwitch = (ImageView) findViewById(R.id.show_in_grid);
        mEmployeeListView = (ListView) findViewById(R.id.listViewEmployeeItems);
        //set the listeners
        listSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchListIcon.setImageResource(R.drawable.ic_view_module_black_36dp);
                setLinear();
            }
        });


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(R.layout.actionbar_view_change);
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM); //show it


        gridSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchListIcon.setImageResource(R.drawable.ic_view_list_black_36dp);
                setGrid();
            }
        });

        //set the adapters
        adapter = new EmployeeListAdapter(EmployeeListingActivity.this, DataHolder.getEmployeeListFromDepartment(departmentId), false);
        mEmployeeListView.setAdapter(adapter);
        mEmployeeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Employee employee = (Employee) mEmployeeListView.getAdapter().getItem(position);
                Intent i = new Intent(EmployeeListingActivity.this, EmployeeDetailActivity.class);
                i.putExtra("employee", employee);
                startActivity(i);
            }
        });

    }

    public void setLinear() {
        adapter = new EmployeeListAdapter(EmployeeListingActivity.this, DataHolder.getEmployeeListFromDepartment(departmentId), false);
        mEmployeeListView.setAdapter(adapter);
        mIsGrid = false;
    }

    public void setGrid() {
        FlipSettings settings = new FlipSettings.Builder().defaultPage(1).build();
        flipAdapter = new EmployeeFlipAdapter(EmployeeListingActivity.this, DataHolder.getEmployeeListFromDepartment(departmentId), settings);
        mEmployeeListView.setAdapter(flipAdapter);
        mIsGrid = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_buttons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                if (!mIsGrid) {
                    item.setIcon(getResources().getDrawable(R.drawable.ic_view_list_black_36dp));
                    setGrid();
                } else {
                    item.setIcon(getResources().getDrawable(R.drawable.ic_view_module_black_36dp));
                    setLinear();
                }
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
