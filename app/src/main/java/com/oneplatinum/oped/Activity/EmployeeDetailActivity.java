package com.oneplatinum.oped.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.oneplatinum.oped.R;
import com.oneplatinum.oped.model.Employee;
import com.oneplatinum.oped.utils.Utils;

public class EmployeeDetailActivity extends AppCompatActivity {

    ImageView image, image_bg;
    TextView name, designation, address, phone, dob, email, gender;
    RelativeLayout imageContainer;
    AQuery aq;
    private float mDelta = 0;
    private float mLastPoint = 0;
    private int mMinImageHeight, mMaxImageHeight, mScreenWidth;
    int updatedHeight;
    float relativeAlpha = 0.4f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);

        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mMaxImageHeight = mScreenWidth;
//        mMaxImageHeight = Utils.dpToPx(EmployeeDetailActivity.this, 360);
        mMinImageHeight = Utils.dpToPx(EmployeeDetailActivity.this, 240);
        updatedHeight = mMinImageHeight;

        //actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aq = new AQuery(EmployeeDetailActivity.this);
        //get the employee info
        final Employee mEmployee = (Employee) getIntent().getSerializableExtra("employee");

        //initialise the views
        image = (ImageView) findViewById(R.id.user_image);
        name = (TextView) findViewById(R.id.user_name);
        designation = (TextView) findViewById(R.id.user_designation);
        address = (TextView) findViewById(R.id.user_address);
        phone = (TextView) findViewById(R.id.user_phone);
        gender = (TextView) findViewById(R.id.user_gender);
        email = (TextView) findViewById(R.id.user_email);
        dob = (TextView) findViewById(R.id.user_dob);
        image_bg = (ImageView) findViewById(R.id.user_image_bg);
        imageContainer = (RelativeLayout) findViewById(R.id.top_image_container);
        image_bg.setAlpha(0.7f);
        image_bg.setTag(true);
        //set the views
        aq.id(image).image(Utils.getResizedImagesImagesUrl(mEmployee.image), true, true);
        aq.id(image_bg).image(Utils.getResizedImagesImagesUrl(mEmployee.image), true, true, 0, 0, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                super.callback(url, iv, bm, status);
                //scale the back image to top crop
                Matrix matrix = image_bg.getImageMatrix();
                float imageWidth = bm.getWidth();
                float scaleRatio = mScreenWidth / imageWidth;
                matrix.postScale(scaleRatio, scaleRatio);
//                float imageHeight = bm.getHeight() * scaleRatio;
                image_bg.setImageMatrix(matrix);
                image_bg.getLayoutParams().height = mScreenWidth;
            }
        });

        name.setText(mEmployee.name);
        designation.setText(mEmployee.designation);
        address.setText(mEmployee.address);
        gender.setText(mEmployee.gender);
        email.setText(mEmployee.email);
        dob.setText(mEmployee.dob);
        phone.setText(mEmployee.phone);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = mEmployee.phone;
                Intent in = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
                try {
                    startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(EmployeeDetailActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        image_bg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mDelta = 0;
                        mLastPoint = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        mLastPoint = 0;
                        mDelta = 0;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mDelta = event.getY() - mLastPoint;
                        mLastPoint = event.getY();
                        if (Math.abs(mDelta) > 5) {
//                            slop check
                            invalidateImageContainer();
                            Log.d("delta", mDelta + "");
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        image_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do nothing, only register to get touch events
            }
        });
    }

    public void invalidateImageContainer() {
        int delta = (int) mDelta;
        Log.d("updatedHeight", updatedHeight + "_with_delta" + (updatedHeight + delta));
        updatedHeight += delta;
        LinearLayout.LayoutParams rlp;
        if (updatedHeight < mMaxImageHeight && updatedHeight > mMinImageHeight) {
            rlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, updatedHeight);
            imageContainer.setLayoutParams(rlp);
            //change the alpha
            relativeAlpha = (float) ((updatedHeight - 0.4f) * (0.6f / (mMaxImageHeight - mMinImageHeight)));
            Log.d("relativeAlpha", relativeAlpha + "");
            image_bg.setAlpha(relativeAlpha);
            return;
        } else if (updatedHeight >= mMaxImageHeight) {
            rlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, mMaxImageHeight);
            imageContainer.setLayoutParams(rlp);
            image_bg.setAlpha(1.0f);
            return;
        } else if (updatedHeight <= mMinImageHeight) {
            rlp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, mMinImageHeight);
            imageContainer.setLayoutParams(rlp);
            image_bg.setAlpha(0.4f);
            return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
