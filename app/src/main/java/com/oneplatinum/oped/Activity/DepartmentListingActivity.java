package com.oneplatinum.oped.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.oneplatinum.oped.R;
import com.oneplatinum.oped.data.DataHolder;
import com.oneplatinum.oped.model.Department;
import com.oneplatinum.oped.utils.ParseUtility;
import com.oneplatinum.oped.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class DepartmentListingActivity extends AppCompatActivity {

    SwipeRefreshLayout mSwipeLayout;
    LinearLayout departmentList;
    AQuery aq;
    DisplayMetrics dm = new DisplayMetrics();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_department);

        aq = new AQuery(DepartmentListingActivity.this);

        //initialise views
        initViews();
    }

    public void initViews() {
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_root);
        departmentList = (LinearLayout) findViewById(R.id.department_container);

        //set refresh listener to refresh the list
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //call the api to fetch the list
                fetchData();
            }
        });

        //get the window height and width
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        //populate the item views
        populateViews();
    }

    public void populateViews() {
        //cleat all views from the container
        departmentList.removeAllViews();
        //item view height
        int height = dm.heightPixels / 5;

        //get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(DepartmentListingActivity.this);
        //loop for all the data in the datalist
        for (int i = 0; i < DataHolder.departmentList.size(); i++) {
            Department mDepartment = DataHolder.departmentList.get(i);
            final View itemView = inflater.inflate(R.layout.department_item_layout, null);
            ((TextView) itemView.findViewById(R.id.text)).setText(mDepartment.name);
            aq.id(itemView.findViewById(R.id.image)).image(Utils.getResizedImagesImagesUrl(mDepartment.image), true, true);
            itemView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height));
            itemView.setTag(mDepartment.name);
            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            itemView.findViewById(R.id.filter).setAlpha(0.3f);
                            break;
                        case MotionEvent.ACTION_UP:
                            itemView.findViewById(R.id.filter).setAlpha(1);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            itemView.findViewById(R.id.filter).setAlpha(0.3f);
                            break;
                        default:
                            itemView.findViewById(R.id.filter).setAlpha(1);
                            break;
                    }

                    //consume the touch event
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //goto department listing activity
                    showDepartmentEmployees((String) v.getTag());
                }
            });
            departmentList.addView(itemView);
        }
    }

    public void showDepartmentEmployees(String id) {
        Intent i = new Intent(DepartmentListingActivity.this, EmployeeListingActivity.class);
        i.putExtra("departmentId", id);
        startActivity(i);
    }

    public void fetchData() {
        if (Utils.isNetworkAvailable(DepartmentListingActivity.this)) {
            mSwipeLayout.setRefreshing(true);
            aq.ajax(DataHolder.fetchUrl, JSONObject.class, fetchCallback());
        } else {
            Toast.makeText(DepartmentListingActivity.this, "Please connect to Internet", Toast.LENGTH_LONG).show();
        }
    }

    public AjaxCallback<JSONObject> fetchCallback() {
        return new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                if (object != null) {
                    try {
                        DataHolder.departmentList = ParseUtility.parseDepartment(object);
                        populateViews();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mSwipeLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}
