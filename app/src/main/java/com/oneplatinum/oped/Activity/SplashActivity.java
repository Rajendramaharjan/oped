package com.oneplatinum.oped.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.oneplatinum.oped.R;
import com.oneplatinum.oped.data.DataHolder;
import com.oneplatinum.oped.utils.ParseUtility;
import com.oneplatinum.oped.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    ImageView logoImage;
    ProgressBar mProgressBar;
    LinearLayout statusView;
    TextView statusMessage;
    private boolean mHasFetched = false;
    SharedPreferences shp;
    SharedPreferences.Editor editor;
    String SHAREDPREFERENCES_NAME = "oped";
    String SHAREDPREFERENCES_RESPONSE ="response";
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //request fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        aq = new AQuery(SplashActivity.this);
        shp = getSharedPreferences(SHAREDPREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = shp.edit();

        //initialise the views
        initViews();

        //fetch the data here and set the progress message snd show the progress here
        fetchData();

//        //get the mock data
//        DataHolder.departmentList = Utils.getSampleData(SplashActivity.this);
//        //goto department activity after a time interval
//        gotoDepartmentTask();
    }

    public void initViews() {
        logoImage = (ImageView) findViewById(R.id.logo_image);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        statusView = (LinearLayout) findViewById(R.id.status_show);
        statusMessage = (TextView) findViewById(R.id.status_msg);

        statusMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mHasFetched) {
                    //fetch the data again
                    fetchData();
                }
            }
        });

        //keep the status view invisible
        statusView.setVisibility(View.GONE);
    }

    public void gotoDepartment() {
        Intent intent = new Intent(SplashActivity.this, DepartmentListingActivity.class);
        startActivity(intent);
        finish();
    }

    public void showLoading() {
        statusView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        statusMessage.setText("Loading..");
    }

    public void hideStatus() {
        statusView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        statusMessage.setText("");
    }

    public void showError(String message) {
        statusView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        statusMessage.setText(message);
    }

    public void gotoDepartmentTask() {
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, DepartmentListingActivity.class));
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(t, 3000);
    }

    public void fetchData() {
        if (Utils.isNetworkAvailable(SplashActivity.this)) {
            showLoading();
            aq.ajax(DataHolder.fetchUrl, JSONObject.class, fetchCallback());
        } else {
            String responseString = shp.getString(SHAREDPREFERENCES_RESPONSE, "");
            if (responseString.length() == 0) {
                showError(getResources().getString(R.string.no_internet));
            } else {
                try {
                    DataHolder.departmentList = ParseUtility.parseDepartment(new JSONObject(responseString));
                    hideStatus();
                    gotoDepartment();
                } catch (JSONException e) {
                    e.printStackTrace();
                    showError(getResources().getString(R.string.no_internet));
                }
            }
        }
    }

    public AjaxCallback<JSONObject> fetchCallback() {
        return new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                if (object != null) {
                    editor.putString(SHAREDPREFERENCES_RESPONSE, object.toString()).commit();
                    try {
                        DataHolder.departmentList = ParseUtility.parseDepartment(object);
                        hideStatus();
                        gotoDepartment();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showError(getResources().getString(R.string.error_in_connection));
                    }
                } else {
                    showError(getResources().getString(R.string.error_in_connection));
                }
            }
        };
    }
}
