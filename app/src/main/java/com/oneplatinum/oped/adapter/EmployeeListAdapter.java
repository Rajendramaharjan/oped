package com.oneplatinum.oped.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.oneplatinum.oped.R;
import com.oneplatinum.oped.model.Employee;
import com.oneplatinum.oped.utils.Utils;

import java.util.ArrayList;

/**
 * Created by user on 3/31/2017.
 */
public class EmployeeListAdapter extends BaseAdapter {

    Context context;
    ArrayList<Employee> list;
    boolean isGrid = true;
    AQuery aq;

    public EmployeeListAdapter(Context context, ArrayList<Employee> list, boolean isGrid) {
        super();
        this.context = context;
        this.list = list;
        this.isGrid = isGrid;
        aq = new AQuery(context);
    }

//    @Override
////    public RecyclerView.VHolder.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//    public EmployeeListAdapter.VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view;
//        if (isGrid) {
//            view = LayoutInflater.from(context).inflate(R.layout.employee_detail_grid_layout, null);
//        } else {
//            view = LayoutInflater.from(context).inflate(R.layout.employee_detail_list_layout, null);
//        }
//        return new VHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(final VHolder vHolder, int position) {
////        VHolder vHolder = (VHolder) holder;
//        final int pos = position;
//        View.OnClickListener cListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(context, EmployeeDetailActivity.class);
//                i.putExtra("employee", list.get(pos));
//                context.startActivity(i);
//            }
//        };
////        vHolder.userImage.setImageResource(R.drawable.drawable_1);
//        vHolder.userName.setText(list.get(position).name);
//        vHolder.designation.setText(list.get(position).designation);
////        aq.id(vHolder.userImage).image(Utils.getResizedImagesImagesUrl(list.get(position).image), true, true);
//
//        aq.id(vHolder.userImage).image(Utils.getResizedImagesImagesUrl(list.get(position).image), true, true, 0, 0,
//                new BitmapAjaxCallback() {
//                    protected void callback(String url, ImageView iv,
//                                            Bitmap bm, AjaxStatus status) {
//                        if (bm != null) {
//                            vHolder.userImage.setScaleType(ImageView.ScaleType.FIT_XY);
//                            vHolder.userImage.setImageBitmap(bm);
//                        } else {
//                            vHolder.userImage.setImageResource(R.drawable.optn_100);
//                        }
//                    }
//                });
//
////        if (list.get(position).image != null && list.get(position).image.length() > 0) {
////            aq.id(vHolder.userImage).image(Utils.getResizedImagesImagesUrl(list.get(position).image));
////        } else {
////            aq.id(vHolder.userImage).image(Utils.getResizedImagesImagesUrl(list.get(position).image), true, true, 400,
////                    R.drawable.back2
////            );
////        }
//
//        vHolder.userImage.setOnClickListener(cListener);
//        vHolder.userName.setOnClickListener(cListener);
//        vHolder.designation.setOnClickListener(cListener);
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.employee_detail_list_layout, parent, false);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.userName = (TextView) convertView.findViewById(R.id.user_name);
        holder.designation = (TextView) convertView.findViewById(R.id.designationTXT);
        aq.id(holder.userImage).image(Utils.getResizedImagesImagesUrl(list.get(position).image), true, true, 0, 0,
                new BitmapAjaxCallback() {
                    protected void callback(String url, ImageView iv,
                                            Bitmap bm, AjaxStatus status) {
                        if (bm != null) {
                            holder.userImage.setImageBitmap(bm);
                        } else {
                            holder.userImage.setImageResource(R.drawable.optn_100);
                        }
                    }
                });

        return convertView;
    }

    public class ViewHolder {

        public ImageView userImage;
        public TextView userName, designation;

        public ViewHolder(View v) {
            userImage = (ImageView) v.findViewById(R.id.user_image);
            userName = (TextView) v.findViewById(R.id.user_name);
            designation = (TextView) v.findViewById(R.id.user_designation);
        }
    }

//    public class VHolder extends RecyclerView.ViewHolder {
//
//        public ImageView userImage;
//        public TextView userName, designation, address;
//
//        public VHolder(View v) {
//            super(v);
//            userImage = (ImageView) v.findViewById(R.id.user_image);
//            userName = (TextView) v.findViewById(R.id.user_name);
//            designation = (TextView) v.findViewById(R.id.user_designation);
////            address = (TextView) v.findViewById(R.id.user_address);
////            address.setVisibility(View.GONE);
//        }
//    }

//    public void setLayoutType(boolean isGrid) {
//        this.isGrid = isGrid;
//    }
}
