package com.oneplatinum.oped.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.oneplatinum.oped.R;
import com.oneplatinum.oped.model.Employee;
import com.oneplatinum.oped.utils.Utils;
import com.yalantis.flipviewpager.adapter.BaseFlipAdapter;
import com.yalantis.flipviewpager.utils.FlipSettings;

import java.util.List;
import java.util.Random;

/**
 * Created by Rajen on 5/4/2017.
 */

public class EmployeeFlipAdapter extends BaseFlipAdapter<Employee> {

    List<Employee> mTotalItems;
    Context mContext;
    AQuery mAQuery;
    RelativeLayout pagerEmployeeView;

    private final int PAGES = 3;
    private int[] IDS_INTEREST = {R.id.designationTXT};

    public EmployeeFlipAdapter(Context context, List<Employee> items, FlipSettings settings) {
        super(context, items, settings);
        mAQuery = new AQuery(context);
        this.mTotalItems = items;
        this.mContext = context;

    }

    @Override
    public View getPage(int position, View convertView, ViewGroup parent, Employee Lemployee, Employee Remployee) {
        final EmployeeHolder holder;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        if (convertView == null) {
            holder = new EmployeeHolder();
            convertView = inflater.inflate(R.layout.single_row_span, parent, false);
            holder.leftAvatar = (ImageView) convertView.findViewById(R.id.first);
            holder.rightAvatar = (ImageView) convertView.findViewById(R.id.second);
            holder.infoPage = inflater.inflate(R.layout.single_item_info_flipped, parent, false);
            holder.nickName = (TextView) holder.infoPage.findViewById(R.id.user_name);
            holder.designation = (TextView) holder.infoPage.findViewById(R.id.designationTXT);
            holder.pagerEmployeeView = (RelativeLayout) holder.infoPage.findViewById(R.id.pagerEmployeeView);

            convertView.setTag(holder);
        } else {
            holder = (EmployeeHolder) convertView.getTag();
        }

        holder.pagerEmployeeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "HEEEOELEOE", Toast.LENGTH_SHORT).show();
            }
        });

        switch (position) {
//                 Merged page with 2 Employee
            case 1:
                String LimageURL = Lemployee.getImage();
                mAQuery.id(holder.leftAvatar).image(Utils.getResizedImagesImagesUrl(LimageURL), true, true, 0, 0,
                        new BitmapAjaxCallback() {
                            protected void callback(String url, ImageView iv,
                                                    Bitmap bm, AjaxStatus status) {
                                if (bm != null) {
                                    holder.leftAvatar.setImageBitmap(bm);
                                    Palette p = new Palette.Builder(bm).generate();

//                                    Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
//                                        public void onGenerated(Palette p) {
                                    // Use generated instance
                                    Palette.Swatch vibrantSwatch = checkVibrantSwatch(p);
                                    if (vibrantSwatch != null) {
                                        Log.d("VIBRANT", "onGenerated: VIBRANT SWATCH" + vibrantSwatch.getRgb());
//                                        holder.infoPage.setBackgroundColor(vibrantSwatch.getBodyTextColor());
                                        holder.pagerEmployeeView.setBackgroundColor(vibrantSwatch.getRgb());
                                    }
//                                        }
//                                    });
                                } else {
                                    holder.leftAvatar.setImageResource(R.drawable.optn_100);
//                                    holder.infoPage.setBackgroundColor(color);
                                }
                            }
                        });

                if (Remployee != null) {
//                    holder.rightAvatar.setImageResource(((Employee) Remployee).getAVATAR());

                    String RimageURL = Remployee.getImage();
//                    mAQuery.id(holder.rightAvatar).image(Utils.getResizedImagesImagesUrl(RimageURL), true, true, 0, 0,
                    mAQuery.id(holder.rightAvatar).image(Utils.getResizedImagesImagesUrl(RimageURL), true, true, 0, 0,
                            new BitmapAjaxCallback() {
                                protected void callback(String url, ImageView iv,
                                                        Bitmap bm, AjaxStatus status) {
                                    if (bm != null) {
                                        holder.rightAvatar.setImageBitmap(bm);

                                        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                                            public void onGenerated(Palette p) {
                                                // Use generated instance
                                                Palette.Swatch vibrantSwatch = checkVibrantSwatch(p);
                                                if (vibrantSwatch != null) {
//                                                    holder.infoPage.setBackgroundColor(vibrantSwatch.getRgb());
                                                    holder.pagerEmployeeView.setBackgroundColor(vibrantSwatch.getRgb());
                                                }
                                            }
                                        });

                                    } else {
                                        holder.rightAvatar.setImageResource(R.drawable.optn_100);
//                                        holder.infoPage.setBackgroundColor(color);
                                    }
                                }
                            });
                }
                break;
            default:
                fillHolder(holder, position == 0 ? Lemployee : Remployee);
                holder.infoPage.setTag(holder);
                return holder.infoPage;
        }
        return convertView;
    }

    @Override
    public int getPagesCount() {
        return PAGES;
    }

    private void fillHolder(EmployeeHolder holder, Employee employee) {
        if (employee == null)
            return;
        TextView iViews = holder.designation;
        String iInterests = employee.getDesignation();
        iViews.setText(iInterests);

//        Random rnd = new Random();
//        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//
//        holder.infoPage.setBackgroundColor(color);
        holder.nickName.setText(employee.getName());

    }

    // Generate palette asynchronously and use it on a different
// thread using onGenerated()
    public void createPaletteAsync(Bitmap bitmap) {
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette p) {
                // Use generated instance
            }
        });
    }

    Palette createPaletteSync(Bitmap bm) {
        Palette p = Palette.from(bm).generate();
        return p;
    }

    // Return a palette's vibrant swatch after checking that it exists
//    private Palette.Swatch checkVibrantSwatch(Palette p) {
    private Palette.Swatch checkVibrantSwatch(Palette p) {
        Palette.Swatch vibrant = p.getVibrantSwatch();
        int vibrantC = p.getVibrantColor(000000);
        Log.d("VIBRANT II", "checkVibrantSwatch: " + vibrantC);
        if (vibrant != null) {
            return vibrant;
        } else {
            // Throw error
            return null;
        }
    }

    class EmployeeHolder {
        ImageView leftAvatar;
        ImageView rightAvatar;
        View infoPage;

        TextView designation;
        TextView nickName;
        RelativeLayout pagerEmployeeView;
    }
}
