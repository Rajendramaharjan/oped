package com.oneplatinum.oped.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;

import com.oneplatinum.oped.data.DataHolder;
import com.oneplatinum.oped.model.Department;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 3/31/2017.
 */
public class Utils {

    public static Boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityMgr.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected())
            return true;

        return false;
    }

    public static ArrayList<Department> getSampleData(Context context) {
        ArrayList<Department> list;
//        String sampleJsonString = context.getResources().getString(R.string.sample_json);
        String sampleJsonString = DataHolder.STRING_SAMPLE;
        StringBuffer buffer = new StringBuffer(sampleJsonString);
        try {
            JSONObject object = new JSONObject(buffer.toString());
            list = ParseUtility.parseDepartment(object);
        } catch (JSONException e) {
            e.printStackTrace();
            return new ArrayList<Department>();
        }
        Log.d("sampleJson", sampleJsonString + "__" + list + "");
        return list;
    }


    public static String getResizedImagesImagesUrl(String url) {
        if (url.contains("https://graph.facebook.com/")) {
            return url;
        }

        String changedURL = url.replaceAll("image/upload/", "image/upload/w_420,h_400,c_fill/");
        return changedURL;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        int px = Math.round(dp
                * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
