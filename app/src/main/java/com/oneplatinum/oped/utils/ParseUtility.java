package com.oneplatinum.oped.utils;

import com.oneplatinum.oped.model.Department;
import com.oneplatinum.oped.model.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by user on 4/1/2017.
 */
public class ParseUtility {

    public static ArrayList<Department> parseDepartment(JSONObject object) throws JSONException {
        ArrayList<Department> list = new ArrayList<Department>();
//        JSONObject output = object.getJSONObject("output");
//        JSONObject response = object.getJSONArray("response");
        JSONArray departmentArray = object.getJSONArray("response");
        for (int i = 0; i < departmentArray.length(); i++) {
            Department mDepartment = new Department();
            JSONObject mDepartmentObject = departmentArray.getJSONObject(i);
            mDepartment.name = mDepartmentObject.getString("departmentName");
            mDepartment.image = mDepartmentObject.getString("departmentImage");
            mDepartment.employeeList = parseEmployee(mDepartmentObject.getJSONArray("employee"));
            list.add(mDepartment);
        }
        return list;
    }

    public static ArrayList<Employee> parseEmployee(JSONArray array) throws JSONException {
        ArrayList<Employee> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Employee mEmployee = new Employee();
            JSONObject object = array.getJSONObject(i);
            mEmployee.name = object.getString("Name");
            mEmployee.address = object.getString("Address");
            mEmployee.phone = object.getString("phoneNumber");
            mEmployee.designation = object.getString("Designation");
            mEmployee.email = object.getString("emailAddress");
            mEmployee.image = object.getString("profileImage");
            mEmployee.gender = object.getString("Gender");
            mEmployee.dob = object.getString("dateOfBirth");
            list.add(mEmployee);
        }
        return list;
    }
}
